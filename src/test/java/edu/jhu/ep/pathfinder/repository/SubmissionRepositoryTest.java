package edu.jhu.ep.pathfinder.repository;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import edu.jhu.ep.pathfinder.domain.FileSubmission;
import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;
import edu.jhu.ep.pathfinder.domain.TextSubmission;

@RunWith(SpringRunner.class)
@DataJpaTest
public class SubmissionRepositoryTest {

    @Autowired
    private SubmissionRepository repository;

    @Test
    public void testSaveTextAndFileSubmissionsAndThenFindAll() {

        repository.save(new TextSubmission.Builder()
                .title("hello-world")
                .source("#include <stdio.h> ...")
                .status(Status.SUBMITTED)
                .build());

        repository.save(new FileSubmission.Builder()
                .status(Status.SUBMITTED)
                .build());

        assertEquals("There should be two submissions in the repository", 2,
                repository.count());

        final List<Submission> submissions = new ArrayList<>();
        for (final Submission submission : repository.findAll()) {
            submissions.add(submission);
        }
        assertTrue("One should be a TextSubmission",
                submissions.stream().anyMatch(submission -> submission
                        .getClass().equals(TextSubmission.class)));
        assertTrue("One should be a FileSubmission",
                submissions.stream().anyMatch(submission -> submission
                        .getClass().equals(FileSubmission.class)));
    }

    @Test
    public void testFindOneById() {

        repository.save(new TextSubmission.Builder()
                .title("hello-world")
                .source("#include <stdio.h> ...").status(Status.SUBMITTED)
                .build());

        assertEquals("hello-world",
                ((TextSubmission) repository.findOne(
                        repository.findAll().iterator().next().getId()))
                                .getTitle());
    }

    @Test
    public void testFindByStatus() {

        repository.save(new TextSubmission.Builder()
                .title("hello-world")
                .source("#include <stdio.h> ...")
                .status(Status.SUBMITTED)
                .build());
        repository.save(new TextSubmission.Builder()
                .title("hello-world")
                .source("#include <stdio.h> ...")
                .status(Status.FINGERPRINTED)
                .build());

        final List<Submission> fingerprintedSubmissions = repository
                .findByStatus(Status.FINGERPRINTED);

        assertEquals("There should be one FINGERPRINTED submission", 1,
                fingerprintedSubmissions.size());

        assertEquals("The Status should be FINGERPRINTED",
                Status.FINGERPRINTED,
                fingerprintedSubmissions.get(0).getStatus());

    }

}
