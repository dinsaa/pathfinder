package edu.jhu.ep.pathfinder.web;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.TextSubmission;
import edu.jhu.ep.pathfinder.service.SubmissionService;

@RunWith(SpringRunner.class)
@WebMvcTest(TextSubmissionController.class)
public class TextSubmissionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubmissionService submissionService;

    @Test
    public void testAddTextSubmission() throws Exception {

        final Submission submission = new TextSubmission.Builder()
                .id(1l)
                .title("hello.c")
                .source("#include<stdio.h>")
                .build();

        given(submissionService.save(any(TextSubmission.class)))
                .willReturn((TextSubmission) submission);

        final String json = "{ \"title\": \"hello.c\", "
                + "\"source\":\"#include <stdio.h>\" }";

        mvc.perform(post("/submissions/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isCreated())
                .andExpect(header().string("Location",
                        "http://localhost/submissions/1"))
                .andExpect(
                        content()
                                .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testAddTextSubmissionWithUnsupportedMediaType()
            throws Exception {

        final String json = "{ \"title\": \"hello.c\", "
                + "\"source\":\"#include <stdio.h>\" }";

        mvc.perform(post("/submissions/")
                .contentType(MediaType.TEXT_HTML)
                .content(json))
                .andExpect(status().isUnsupportedMediaType());
    }

    @Test
    public void testAddTextSubmissionWithInvalidRequestBody()
            throws Exception {

        final String json = "{ \"title\": \"missing 'source' attribute\" }";

        mvc.perform(post("/submissions/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(json))
                .andExpect(status().isBadRequest());
    }
}
