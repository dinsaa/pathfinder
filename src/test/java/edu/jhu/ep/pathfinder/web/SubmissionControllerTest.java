package edu.jhu.ep.pathfinder.web;

import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import edu.jhu.ep.pathfinder.domain.FileSubmission;
import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;
import edu.jhu.ep.pathfinder.domain.TextSubmission;
import edu.jhu.ep.pathfinder.repository.SubmissionRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(SubmissionController.class)
public class SubmissionControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private SubmissionRepository repository;

    @Test
    public void testGetSubmissions() throws Exception {

        final List<Submission> submissions = new ArrayList<>();
        submissions.add(new TextSubmission.Builder()
                .id(1l)
                .title("hello.c")
                .source("#include<stdio.h>")
                .status(Status.SUBMITTED)
                .build());
        submissions.add(new FileSubmission.Builder()
                .id(2l)
                .status(Status.SUBMITTED)
                .build());

        given(repository.findAll()).willReturn(submissions);

        mvc.perform(get("/submissions"))
                .andExpect(status().isOk())
                .andExpect(
                        content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    public void testSubmissionsWithMethodNotAllowed() throws Exception {

        mvc.perform(put("/submissions"))
                .andExpect(status().isMethodNotAllowed());
        mvc.perform(delete("/submissions"))
                .andExpect(status().isMethodNotAllowed());
        mvc.perform(patch("/submissions"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testGetTextSubmissionById() throws Exception {

        given(repository.findOne(1l))
                .willReturn(new TextSubmission.Builder()
                        .id(1l)
                        .title("hello.c")
                        .source("#include<stdio.h>")
                        .status(Status.SUBMITTED)
                        .build());

        mvc.perform(get("/submissions/1/"))
                .andExpect(status().isOk())
                .andExpect(
                        content()
                                .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGetFileSubmissionById() throws Exception {

        given(repository.findOne(1l))
                .willReturn(new FileSubmission.Builder()
                        .id(1l)
                        .status(Status.SUBMITTED)
                        .build());

        mvc.perform(get("/submissions/1/"))
                .andExpect(status().isOk())
                .andExpect(
                        content()
                                .contentType(MediaType.APPLICATION_JSON_UTF8));
    }

    @Test
    public void testGetSubmissionByIdNotFound() throws Exception {

        given(repository.findOne(1l)).willReturn(null);

        mvc.perform(get("/submissions/1/"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void testSubmissionByIdWithUnsupportedHttpMethods()
            throws Exception {

        mvc.perform(put("/submissions/1"))
                .andExpect(status().isMethodNotAllowed());
        mvc.perform(delete("/submissions/1"))
                .andExpect(status().isMethodNotAllowed());
        mvc.perform(patch("/submissions/1"))
                .andExpect(status().isMethodNotAllowed());
    }
}
