package edu.jhu.ep.pathfinder.service;

import static org.junit.Assert.*;
import static org.mockito.BDDMockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;
import edu.jhu.ep.pathfinder.domain.TextSubmission;
import edu.jhu.ep.pathfinder.repository.SubmissionRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SubmissionServiceTest {

    @Autowired
    private SubmissionService submissionService;

    @MockBean
    private SubmissionRepository repository;

    @Test
    public void testSubmissionStatusIsInitializedToSubmitted() {

        final Submission submission = new TextSubmission.Builder()
                .title("hello-world.c")
                .source("#include <stdio.h>")
                .status(Status.FINGERPRINTED)
                .build();

        given(repository.save(any(Submission.class))).willReturn(submission);

        final Submission savedSubmission = submissionService
                .save(submission);

        assertEquals(Status.SUBMITTED, savedSubmission.getStatus());
    }

}
