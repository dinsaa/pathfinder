package edu.jhu.ep.pathfinder.web;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.TextSubmission;
import edu.jhu.ep.pathfinder.service.SubmissionService;

/**
 * API end-point for saving {@link TextSubmission}:
 *
 * <pre>
 * - POST /submissions
 * </pre>
 *
 * @author amar
 *
 */
@RestController
@RequestMapping("/submissions")
public class TextSubmissionController {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(TextSubmissionController.class);

    @Autowired
    private SubmissionService service;

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Submission> saveTextSubmission(
            @Valid @RequestBody final TextSubmission submission) {

        LOGGER.debug("Recieved submission: {}", submission);

        final TextSubmission savedSubmission = service.save(submission);

        return ResponseEntity
                .created(
                        ServletUriComponentsBuilder.fromCurrentServletMapping()
                                .path("/submissions/{id}").build()
                                .expand(savedSubmission.getId()).toUri())
                .body(savedSubmission);
    }
}