package edu.jhu.ep.pathfinder.web;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import edu.jhu.ep.pathfinder.domain.FileSubmission;
import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;
import edu.jhu.ep.pathfinder.domain.TextSubmission;
import edu.jhu.ep.pathfinder.repository.SubmissionRepository;

/**
 * Methods returning both {@link TextSubmission} and {@link FileSubmission}:
 *
 * <pre>
 * - GET /submissions?status={SUBMITTED | FINGERPRINTED | DONE}
 *   - 'status' filter parameter is optional
 * - GET /submissions/{id}
 * </pre>
 *
 * @author amar
 *
 */
@RestController
@RequestMapping("/submissions")
public class SubmissionController {

    @Autowired
    private SubmissionRepository repository;

    @RequestMapping(method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Iterable<? extends Submission> getSubmissions(@RequestParam(
            name = "status", required = false) final Status status) {

        if (status != null) {
            return repository.findByStatus(status);
        }
        return repository.findAll();
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Submission getSubmissionById(@PathVariable final Long id) {

        return Optional.ofNullable(repository.findOne(id)).orElseThrow(
                () -> new ResourceNotFoundException("Submission not found"));
    }
}