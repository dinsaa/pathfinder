package edu.jhu.ep.pathfinder.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;

/**
 * Extends Spring's interface enabling CRUD (create, read, update, and delete)
 * database operations for {@link Submission}.
 *
 *
 * @author amar
 *
 * @see <a href=
 *      "http://docs.spring.io/spring-data/jpa/docs/1.10.3.RELEASE/reference/html/">
 *      http://docs.spring.io/spring-data/jpa/docs/1.10.3.RELEASE/reference/html/
 *      </a>
 */
public interface SubmissionRepository
        extends CrudRepository<Submission, Long> {

    List<Submission> findByStatus(final Status status);
}