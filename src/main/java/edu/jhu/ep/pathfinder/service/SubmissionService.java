package edu.jhu.ep.pathfinder.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.jhu.ep.pathfinder.domain.Submission;
import edu.jhu.ep.pathfinder.domain.Submission.Status;
import edu.jhu.ep.pathfinder.repository.SubmissionRepository;

public interface SubmissionService {

    <S extends Submission> S save(S entity);

    @Service
    @Transactional
    public static class SubmissionServiceImpl implements SubmissionService {

        @Autowired
        private SubmissionRepository submissionRepository;

        /**
         * Initialize Submission's status to {@link Status#SUBMITTED} before
         * delegating to {@link SubmissionRepository#save(Submission)}
         */
        @Override
        public <S extends Submission> S save(final S entity) {
            entity.setStatus(Status.SUBMITTED);
            return submissionRepository.save(entity);
        }
    }
}