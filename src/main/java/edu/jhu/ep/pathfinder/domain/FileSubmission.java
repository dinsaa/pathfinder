package edu.jhu.ep.pathfinder.domain;

import java.io.Serializable;

import javax.persistence.Entity;

/**
 * Stub for FileSubmission entity.
 */
@Entity
public class FileSubmission extends Submission implements Serializable {

    private static final long serialVersionUID = 1l;

    public FileSubmission() {
    }

    public static class Builder {

        private Long id;
        private Status status;

        public Builder id(final Long id) {
            this.id = id;
            return this;
        }

        public Builder status(final Status status) {
            this.status = status;
            return this;
        }

        public FileSubmission build() {
            final FileSubmission submission = new FileSubmission();
            submission.setId(id);
            submission.setStatus(status);
            return submission;
        }
    }
}
