package edu.jhu.ep.pathfinder.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Stub for fingerprint entity.
 *
 * XXX: Since {@link Fingerprint}s are kept in a {@link Set} within
 * {@link Submission}, it must implement {@link #hashCode()} and
 * {@link #equals(Object)}.
 *
 */
@Entity
public class Fingerprint implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "submission_id")
    @JsonIgnore
    private Submission submission;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Submission getSubmission() {
        return submission;
    }

    public void setSubmission(final Submission submission) {
        this.submission = submission;
        if (submission.getFingerprints() == null) {
            submission.setFingerprints(new HashSet<>());
        }
        submission.getFingerprints().add(this);
    }
}
