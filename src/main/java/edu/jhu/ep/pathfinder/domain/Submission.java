package edu.jhu.ep.pathfinder.domain;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Base type for a source code submission
 *
 * @author amar
 *
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Submission implements Serializable {

    private static final long serialVersionUID = 1l;

    /**
     * Status of a submission in the system
     */
    public enum Status
    {
        SUBMITTED, FINGERPRINTED, DONE
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private Status status = Status.SUBMITTED;

    @OneToMany(mappedBy = "submission")
    @JsonIgnore
    private Set<Fingerprint> fingerprints;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(final Status status) {
        this.status = status;
    }

    @JsonIgnore
    public Set<Fingerprint> getFingerprints() {
        return fingerprints;
    }

    public void setFingerprints(final Set<Fingerprint> fingerprints) {
        this.fingerprints = fingerprints;
    }

    public void addFingerprint(final Fingerprint fingerprint) {
        if (fingerprints == null)
            fingerprints = new HashSet<>();
        fingerprints.add(fingerprint);
        fingerprint.setSubmission(this);
    }
}
