package edu.jhu.ep.pathfinder.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;

import org.hibernate.validator.constraints.NotBlank;

/**
 * Textual source code submission
 *
 * @author amar
 *
 */
@Entity
public class TextSubmission extends Submission implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotBlank
    @Column
    private String title;

    @NotBlank
    @Column
    @Lob
    private String source;

    public TextSubmission() {
    }

    public TextSubmission(final String title, final String source) {
        this.title = title;
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getSource() {
        return source;
    }

    public void setSource(final String source) {
        this.source = source;
    }

    @Override
    public String toString() {
        return "TextSubmission [id=" + getId() + ", title=" + title
                + ", source=" + source + ", status=" + getStatus() + "]";
    }

    public static class Builder {

        private Long id;
        private String title;
        private String source;
        private Status status;

        public Builder id(final Long id) {
            this.id = id;
            return this;
        }

        public Builder title(final String title) {
            this.title = title;
            return this;
        }

        public Builder source(final String source) {
            this.source = source;
            return this;
        }

        public Builder status(final Status status) {
            this.status = status;
            return this;
        }

        public TextSubmission build() {
            final TextSubmission submission = new TextSubmission(title,
                    source);
            submission.setId(id);
            submission.setStatus(status);
            return submission;
        }
    }
}
