# PATHFINDER #

## Prerequisites ##

* [JDK 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Apache Maven](https://maven.apache.org/)

## Build ##

```
#!bash
$ mvn package
$ java -jar target/pathfinder-0.0.1-SNAPSHOT.jar
```

## Development ##

* To avoid having to manually build and restart the application each time you make a change while developing, 
use Spring Boot's [DevTools](http://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/html/using-boot-devtools.html), 
which automatically restarts the application whenever files on the classpath change. You can start the application in 
your IDE or from the command line using the Spring Boot Maven plugin:

        #!bash
        $ mvn spring-boot:run

## Links ##
* [Spring Boot Reference Guide](http://docs.spring.io/spring-boot/docs/1.4.1.RELEASE/reference/htmlsingle/)
